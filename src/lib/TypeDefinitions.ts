// Target type used in Sidebar menus
export type Target = {
    link: string,
    label: string
};

// Menu type used in Sidebar menus
export type Menu = {
    name: string,
    folding: boolean,
    targets: Target[]
};
