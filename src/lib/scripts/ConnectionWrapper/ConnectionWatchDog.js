// @ts-nocheck
class ConnectionWatchDog {
    constructor() {
        this.set_timeout(1000);
    }

    register_connection_lost_callback(connection_lost_callback) {
        this.connection_lost_callback = connection_lost_callback;
    }

    get_trigger() {
        return () => { this.#handle_received_trigger(); };
    }

    set_timeout(timeout) {
        clearInterval(this.interval);
        this.interval = setInterval(() => { this.#handle_timeout(); }, timeout);
        this.timeout_counter = 0;
    }

    #handle_timeout() {
        this.timeout_counter++;
        if (this.timeout_counter > 10) {
            this.disconnect_counter = DISCONNECT_LIMIT / this.interval;
            try {
                this.timeout_counter = 0;
                this.connection_lost_callback();
            } catch (e) { 
                console.log(e);
            }
        }
    }

    #handle_received_trigger() {
        this.timeout_counter = 0;
    }
};

const DISCONNECT_LIMIT = 10000;

export { ConnectionWatchDog };
