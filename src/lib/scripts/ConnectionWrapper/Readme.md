# ConnectionWrapper

## Instantiation

```js
import { PipedHttpRequest } from "$lib/scripts/ConnectionWrapper/PipedHttpRequest.js";
import { ConnectionWatchDog } from "$lib/scripts/ConnectionWrapper/ConnectionWatchDog.js";

var periNET = {};

periNET.connection = new PipedHttpRequest(5000 /* ms */);
periNET.connection_watchdog = new ConnectionWatchDog();
periNET.connection.set_connectivity_trigger(periNET.connection_watchdog.get_trigger());

export { periNET };
```

## Example

```js
import { periNET } from '$lib/scripts/periNET.js';

function on_connection_error(data) {
    console.log("Error..");
    console.log(data);
}

function on_connection_success(data) {
    console.log("Success..");
    console.log(data);
}

function on_connection_lost() {
    console.log("connection lost..")
}

periNET.connection_watchdog.register_connection_lost_callback(on_connection_lost);
periNET.connection.get("/node", on_connection_success, on_connection_error);
```

__NOTE:__ The `ConnectionWatchDog` is used to determine if the connected server
is still available. This is done by implementing this 10 sec watchdog.
If there is no further succesful request answered within 10 sec after the
last one the `connection_lost_callback` is executed. This can be used for
example to display a *Connection lost* banner.
If the webUI doesn't poll the connection it is recommended to avoid registering
the `connection_lost_callback`.
