// @ts-nocheck
import { SequentialTaskQueue } from "./SequentialTaskQueue.js"

class PipedHttpRequest {
    constructor(request_timeout) {
        this.xhr = new XMLHttpRequest();
        this.task_queue = new SequentialTaskQueue();
        this.request_timeout = request_timeout;
        this.success_watchdog_callback = () => { };
    }

    set_connectivity_trigger(callback) {
        this.success_watchdog_callback = callback;
    }

    patch(path, data, callback, errcallback,timeout) {
        this.send("PATCH", path, data, callback, errcallback,timeout);
    }

    put(path, data, callback, errcallback, timeout) {
        this.send("PUT", path, data, callback, errcallback, timeout);
    }

    get(path, callback, errcallback,timeout) {
        this.send("GET", path, null, callback, errcallback,timeout);
    }

    send(method, path, data, callback, errcallback, timeout) {
        var request = {};
        request.method = method;
        request.path = path;
        request.data = data;
        request.callback = callback;
        request.errcallback = errcallback;
        request.timeout = timeout || this.request_timeout;
        this.#send_request(request);
    }

    #send_request(request) {
        this.task_queue.push(() => {
            return new Promise((resolve, reject) => {
                this.current_request = request;
                this.xhr.open(request.method, request.path);
                this.xhr.timeout = request.timeout;
                this.xhr.onload = () => {
                    try {
                        this.success_watchdog_callback();
                    } catch (e) {
                        console.log(e);
                    }
                    try {
                        if (this.xhr.status >= 200 && this.xhr.status < 300) {
                            request.callback(this.xhr.responseText);
                        } else {
                            request.errcallback(this.xhr);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                    resolve();
                };
                this.xhr.onerror = () => {
                    try {
                        request.errcallback(this.xhr)
                    } catch { }
                    reject();
                };
                this.xhr.onabort = () => {
                    try {
                        request.errcallback(this.xhr)
                    } catch { }
                    reject();
                };
                this.xhr.ontimeout = () => {
                    try {
                        request.errcallback(this.xhr)
                    } catch { }
                    reject();
                };
                try {
                    this.xhr.send(request.data);
                } catch (e) {
                    reject();
                }
            });
        });
    }
};

export { PipedHttpRequest };
