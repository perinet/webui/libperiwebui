import { PipedHttpRequest } from "./ConnectionWrapper/PipedHttpRequest.js";
import { ConnectionWatchDog } from "./ConnectionWrapper/ConnectionWatchDog.js";
import { NodeConfig } from "./Node/NodeConfig.js";
import { NodeProduction } from "./Node/NodeProduction.js"

// namespace to group all perinet related global variables
var periNET = {};

// connection wrapper
periNET.connection = new PipedHttpRequest(5000 /* ms */);
periNET.connection_watchdog = new ConnectionWatchDog();
periNET.connection.set_connectivity_trigger(periNET.connection_watchdog.get_trigger());

// APIs
periNET.node_config = new NodeConfig();
periNET.node_production = new NodeProduction();

export { periNET };
