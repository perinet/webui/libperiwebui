// @ts-nocheck
import { init, addMessages } from 'svelte-i18n';
import generic_translations from './LibTranslations.js';

export function add_translation(key, translation) {
    addMessages(key, translation);
}

const fallbackLocale = 'en';
const initialLocale = localStorage.getItem('language') || fallbackLocale;

addMessages('en', generic_translations.en);
addMessages('de', generic_translations.de);
init({
    fallbackLocale,
    initialLocale
});