// @ts-nocheck
import { periNET } from '$lib/scripts/periNET.js';
/*
{
  "manufacturer": "",
  "mpn": "",
  "serial_number": "",
  "product_version": "",
  "product_variant": "",
  "batch_number": "",
  "product_name": "",
  "country": "DE",
  "hostname": "ui-perimica-bjxt4"
}*/
class NodeProduction {
    #product_name = "";
    #cb_success_product_name;
    #cb_error_product_name;

    #pending = false;

    constructor() {
        this.trigger_get_node_production();
    }

    get_product_name() {
        return new Promise((resolve, reject) => {
            if(this.#product_name.length > 0) {
                resolve(this.#product_name);
            } else if(!this.#pending) {
                this.trigger_get_node_production();
                this.#cb_error_product_name = reject;
                this.#cb_success_product_name = resolve;
            } else {
                this.#cb_error_product_name = reject;
                this.#cb_success_product_name = resolve;
            }
        });
    }

    trigger_get_node_production() {
        this.#pending = true;
        periNET.connection.get("/node/production", cb_get_success, cb_get_error);
    }

    parse_info(data) {
        let json = JSON.parse(data);
        try {
            this.#product_name = json.product_name;
            this.#cb_success_product_name(this.#product_name);
        } catch { }
        this.#pending = false;
    }

    parse_error(data) {
        try {
            this.#cb_error_product_name(data);
        } catch { }
        this.#pending = false;
    }
};

export { NodeProduction };

function cb_get_success(data) {
    periNET.node_production.parse_info(data);
}

function cb_get_error(data) {
    periNET.node_production.parse_error(data);
}
