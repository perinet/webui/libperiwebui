// @ts-nocheck
import { periNET } from '$lib/scripts/periNET.js';

class NodeConfig {
    #app_name = "";
    #app_success;
    #app_error;

    #elem_name = "";
    #elem_success;
    #elem_error;

    #pending = false;

    constructor() {
        this.trigger_get_node_config();
    }

    get_application_name() {
        return new Promise((resolve, reject) => {
            if(this.#app_name.length > 0) {
                resolve(this.#app_name);
            } else if(!this.#pending) {
                this.trigger_get_node_config();
                this.#app_error = reject;
                this.#app_success = resolve;
            } else {
                this.#app_error = reject;
                this.#app_success = resolve;
            }
        });
    }

    set_application_name(app_name) {
        this.#app_name = app_name;
        data = JSON.stringify({
            application_name: app_name
        });
        this.#patch_config(data);
    }

    get_element_name() {
        return new Promise((resolve, reject) => {
            if(this.#elem_name.length > 0) {
                resolve(this.#elem_name);
            } else if(!this.#pending) {
                this.trigger_get_node_config();
                this.#elem_error = reject;
                this.#elem_success = resolve;
            } else {
                this.#elem_error = reject;
                this.#elem_success = resolve;
            }
        });
    }

    set_element_name(element_name) {
        this.#elem_name = element_name;
        data = JSON.stringify({
            element_name: element_name
            });
        this.#patch_config(data);
    }

    #patch_config(data) {
        periNET.connection.patch("/node/config", data, cb_set_success, cb_set_error);
    }

    trigger_get_node_config() {
        this.#pending = true;
        periNET.connection.get("/node/config", cb_get_success, cb_get_error);
    }

    parse_config(data) {
        let json = JSON.parse(data);
        try {
            this.#app_name = json.application_name;
            this.#app_success(this.#app_name);
        } catch { }
        try {
            this.#elem_name = json.element_name;
            this.#elem_success(this.#elem_name);
        } catch { }
        this.#pending = false;
    }

    parse_error(data) {
        try {
            this.#app_error(data);
        } catch { }
        try {
            this.#elem_error(data);
        } catch { }
        this.#pending = false;
    }
};

export { NodeConfig };

function cb_get_success(data) {
    periNET.node_config.parse_config(data);
}

function cb_get_error(data) {
    periNET.node_config.parse_error(data);
}

function cb_set_success(data) {
    console.log(data);
}

function cb_set_error(data) {
    console.log(data);
}
